---
- name: Container Setup

  vars:
    lxc_base: ansible-alpine3.11
    lxc_name: paperless

  import_playbook: ../common/tasks/lxc.yml

- name: Installing Paperless
  hosts: paperless

  vars_files:
    - ../variables.yml

  tasks:

  - name: Ensuring group exists
    group:
      name: "{{ paperless_user }}"
      state: present

  - name: Creating user and making home directory
    user:
      system: yes
      state: present
      name: "{{ paperless_user }}"
      groups: "{{ paperless_user }}"
      home: "{{ paperless_home }}"

  - name: Including timezone setup
    include: ../common/tasks/alpine/timezone.yml

  - name: Installing the required packages
    apk:
      state: present
      update_cache: yes
      name:
        - sudo
        - git
        - supervisor
        - syncthing
        - optipng
        - unpaper
        - imagemagick
        - tesseract-ocr
        - nginx
        - acl
        - gnupg
        - qpdf
        - libxslt
        - poppler
        - libmagic
        - postgresql-dev
        - gcc
        - g++
        - python3-dev
        - poppler-dev
        - jpeg-dev
        - qpdf-dev
        - libxslt-dev

  - name: Creating /dev/shm as ocrmypdf fails silently without this directory (semlock)
    file:
      path: /dev/shm
      state: directory
      mode: '0777'

  - name: Creating directories
    file:
      path: "{{ item }}"
      state: directory
      owner: "{{ paperless_user }}"
      group: "{{ paperless_user }}"
    with_items:
      - /opt/ocrmypdf

  - block:

    - name: Cloning application source
      git:
        repo: "{{ paperless_repository }}"
        dest: "{{ paperless_home }}"
        version: "{{ paperless_version }}"

    - name: Installing application requirements
      shell: |
        python3 -m venv .
        . bin/activate
        pip install --upgrade pip
        pip install --requirement requirements.txt
      args:
        chdir: "{{ paperless_home }}"
        creates: bin/activate

    - name: Migrating database
      shell: |
        . bin/activate
        src/manage.py migrate
      args:
        chdir: "{{ paperless_home }}"
        creates: data/db.sqlite3

    - name: Generating static assets
      shell: |
        . bin/activate
        src/manage.py collectstatic
      args:
        chdir: "{{ paperless_home }}"
        creates: static

    - name: Installing ocrmypdf
      shell: |
        python3 -m venv .
        . bin/activate
        pip install --upgrade pip
        pip install -Iv ocrmypdf=={{ paperless_ocrmypdf_version }}
      args:
        chdir: /opt/ocrmypdf
        creates: bin/activate

    become: true
    become_user: "{{ paperless_user }}"

  - name: Creating syncthing consumption folder permissions
    file:
      path: "{{ paperless_home }}/consumption"
      state: directory
      owner: "{{ paperless_user }}"
      group: syncthing
      mode: '0774'

  - name: Setting read/write consumption folder access
    acl:
      path: "{{ paperless_home }}/consumption"
      etype: other
      permissions: rw
      default: yes
      state: present

  - name: Placing configuration files
    template:
      src: "{{ item | basename }}.j2"
      dest: "{{ item }}"
    with_items:
      - /etc/hostname
      - /etc/supervisord.conf
      - /etc/paperless.conf
      - /opt/paperless/scripts/pre-consumption.sh
      - /etc/nginx/nginx.conf

  - name: Setting pre consumption script executable
    file:
      path: /opt/paperless/scripts/pre-consumption.sh
      mode: '0755'

  - name: Enabling and starting services
    service:
      name: "{{ item }}"
      state: restarted
      enabled: yes
    with_items:
      - nginx
      - supervisord
      - syncthing
    changed_when: false

  - name: Waiting for supervisor to become active
    wait_for:
      port: 9100

  - name: Ensuring paperless has been started
    supervisorctl:
      name: "{{ item }}"
      state: restarted
    with_items:
      - paperless
      - paperless-consumer
    changed_when: false

  - name: Cleaning up packages
    apk:
      state: absent
      name:
        - postgresql-dev
        - gcc
        - g++
        - python3-dev
        - poppler-dev
        - jpeg-dev
        - qpdf-dev
        - libxslt-dev
    changed_when: false
