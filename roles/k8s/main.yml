---
- name: Install Kubernetes Cluster (centos8)
  hosts: k8s_master, k8s_node

  vars_files:
    - ../variables.yml

  tasks:

  - name: Ensuring group exists
    group:
      name: "{{ k8s_user }}"
      state: present

  - name: Creating user and making home directory
    user:
      system: yes
      state: present
      name: "{{ k8s_user }}"
      groups: "{{ k8s_user }}"
      home: "{{ k8s_home }}"

  - name: Adding Docker repository
    get_url:
      url: https://download.docker.com/linux/centos/docker-ce.repo
      dest: /etc/yum.repos.d/

  - name: Installing Docker
    yum:
      state: present
      update_cache: yes
      name:
        - docker-ce-3:18.09.1-3.el7
        - docker-ce-cli-1:18.09.1-3.el7
        - containerd.io-1.2.0-3.el7
        - sudo
        - tc
        - git

  - name: Creating docker directory
    file:
      path: /etc/docker
      state: directory
      mode: '0755'

  - name: Copying Docker daemon configuration
    copy:
      src: daemon.json
      dest: /etc/docker/daemon.json
      owner: root
      group: root
      mode: '0644'
    register: dockerDaemonConfig

  - name: Creating containerd override directory
    file:
      path: /etc/systemd/system/containerd.service.d/
      state: directory
      mode: '0755'

  - name: Copying containerd override configuration
    copy:
      src: containerd-override.conf
      dest: /etc/systemd/system/containerd.service.d/override.conf
      owner: root
      group: root
      mode: '0644'

  - name: Reloading systemd daemon
    systemd:
      daemon_reload: yes

  - name: Ensuring Docker is enabled and started
    systemd:
      name: docker
      state: started
      enabled: yes

  - name: Wait for Docker
    wait_for:
      path: /var/run/docker.pid
      state: present

  - name: Restarting Docker due to daemon config change
    systemd:
      name: docker
      state: restarted
    when: dockerDaemonConfig.changed

  - name: Adding Kubernetes repository
    yum_repository:
      name: kubernetes
      file: kubernetes
      description: Kubernetes Repo
      baseurl: https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
      gpgkey: https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
      gpgcheck: yes

  - name: Installing kubelet and kubeadm on worker nodes
    yum:
      state: present
      update_cache: yes
      name:
        - kubelet-{{ k8s_version }}-0
        - kubeadm-{{ k8s_version }}-0

  - name: Copying kubelet environment file
    copy:
      src: kubelet
      dest: /etc/sysconfig/kubelet
      mode: '0644'

  - name: Enabling kubelet service
    systemd:
      name: kubelet
      enabled: yes

- hosts: k8s_master

  vars_files:
    - ../variables.yml

  tasks:

    - name: Installing kubectl on master node
      yum:
        state: present
        name:
          - kubectl-{{ k8s_version }}-0

    - name: Initializing Kubernetes cluster
      shell: >
        kubeadm init --ignore-preflight-errors all --v=5 --pod-network-cidr=192.168.0.0/16 > k8s_cluster_initialization.log &&
        touch .k8s_cluster_initialized
      args:
        chdir: "{{ k8s_home }}"
        creates: .k8s_cluster_initialized
      register: output
    - debug: var=output

    - name: Extracting cluster join command
      shell: grep -E 'kubeadm join|discovery' k8s_cluster_initialization.log
      args:
        chdir: "{{ k8s_home }}"
      register: join
      changed_when: false
    - debug: var=join

    - name: Calling workers to join cluster
      shell: "{{ join.stdout }} --ignore-preflight-errors all"
      changed_when: false
      delegate_to: "{{ item }}"
      with_items:
        - "{{ groups['k8s_node'][0] }}"
        - "{{ groups['k8s_node'][1] }}"
        - "{{ groups['k8s_node'][2] }}"

    - name: Creating kube folder
      file:
        path: "{{ item }}"
        state: directory
        owner: "{{ k8s_user }}"
        group: "{{ k8s_user }}"
        mode: '0755'
      with_items:
        - "{{ k8s_home }}/.kube"
        - "{{ k8s_home }}/.kube/.check"

    - name: Copying Kubernetes settings
      copy:
        src: /etc/kubernetes/admin.conf
        dest: "{{ k8s_home }}/.kube/config"
        owner: "{{ k8s_user }}"
        group: "{{ k8s_user }}"
        mode: '0644'
        remote_src: yes

    - name: Copying Kubernetes configuration
      copy:
        src: "{{ item }}"
        dest: "{{ k8s_home }}/.kube"
        owner: "{{ k8s_user }}"
        group: "{{ k8s_user }}"
        mode: '0644'
      with_items:
        - calico.yaml
        - k8s-dashboard.yaml
        - k8s-dashboard-admin.yaml
        - k8s-dashboard-binding.yaml

    - block:

      - name: Bringing up pod network
        shell: >
          kubectl apply -f .kube/calico.yaml &&
          touch .kube/.check/.k8s_network_installed
        args:
          chdir: "{{ k8s_home }}"
          creates: .kube/.check/.k8s_network_installed

      - name: Installing Kubernetes Dashboard
        shell: >
          kubectl apply -f .kube/k8s-dashboard.yaml &&
          touch .kube/.check/.k8s_dashboard_installed
        args:
          chdir: "{{ k8s_home }}"
          creates: .kube/.check/.k8s_dashboard_installed

      - name: Creating Kubernetes Dashboard administrator
        shell: >
          kubectl apply -f
          .kube/k8s-dashboard-admin.yaml &&
          touch .kube/.check/.k8s_dashboard_admin_installed
        args:
          chdir: "{{ k8s_home }}"
          creates: .kube/.check/.k8s_dashboard_admin_installed

      - name: Creating Kubernetes Dashboard cluster binding
        shell: >
          kubectl apply -f
          .kube/k8s-dashboard-binding.yaml &&
          touch .kube/.check/.k8s_dashboard_binding_installed
        args:
          chdir: "{{ k8s_home }}"
          creates: .kube/.check/.k8s_dashboard_binding_installed

      - name: Cloning Kubernetes metrics server
        git:
          repo: https://github.com/kubernetes-sigs/metrics-server
          dest: "{{ k8s_home }}/.kube/metrics-server"
          version: 4f6ef77ce0cb128663c86708566b3e43cdc47ac4

      - name: Deploying the metrics server
        shell: >
          kubectl create -f
          .kube/metrics-server/deploy/1.8+ &&
          touch .kube/.check/.k8s_metrics_server_installed
        args:
          chdir: "{{ k8s_home }}"
          creates: .kube/.check/.k8s_metrics_server_installed

      - name: Printing Kubernetes Dashboard token
        shell: >
          kubectl -n kubernetes-dashboard describe secret
          $(kubectl -n kubernetes-dashboard get secret | grep admin-user | awk '{print $1}')
        register: token
        changed_when: false
      - debug: var=token

      become: true
      become_user: "{{ k8s_user }}"
